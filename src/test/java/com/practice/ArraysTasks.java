package test.java.com.practice;

import test.java.com.practice.MinAndMax;

import java.util.Random;

public class ArraysTasks {
    Random random = new Random();

    public void outputArray7Rows7Columns(){
        char[][] array = new char[7][7];
        char outputSymbol = 0x2466;
        for (int i = 0; i < array.length; i++){
            for (int j = 0; j < array[0].length; j++){
                array[i][j] = outputSymbol;
            }
        }
        for (int i = 0; i < array.length; i++){
            for (int j = 0; j < array[i].length; j++){
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    public MinAndMax getMinAndMaxInArray(int[][][] threeDimensionalArray){
        int minValue = threeDimensionalArray[0][0][0];
        int maxValue = threeDimensionalArray[0][0][0];
        for (int i = 0; i < threeDimensionalArray.length; i++){
            for (int j = 0; j < threeDimensionalArray[i].length; j++){
                for (int k = 0; k < threeDimensionalArray[i][j].length; k++){
                    if(threeDimensionalArray[i][j][k] < minValue){
                        minValue = threeDimensionalArray[i][j][k];
                    }
                    if(threeDimensionalArray[i][j][k] > maxValue){
                        maxValue = threeDimensionalArray[i][j][k];
                    }
                }
            }
        }
        MinAndMax result = new MinAndMax(minValue, maxValue);
        return result;
    }

    public int[] sortArrayTask3Rules(int[] array){
        int arrayLength = array.length;
        if (arrayLength == 0){
            return array;
        }
        int[] evenNumbers = new int[arrayLength / 2];
        int[] unevenNumbers = new int[arrayLength / 2];

        int evenNumIndex = 0;
        int unevenNumIndex = 0;

        for(int i = 0; i < arrayLength; i++){
            if (array[i] % 2 == 0){
                evenNumbers[evenNumIndex] = array[i];
                evenNumIndex++;
            }
            else{
                unevenNumbers[unevenNumIndex] = array[i];
                unevenNumIndex++;
            }
        }

        int[] sortedEvenNumbers = sortArrayBubble(evenNumbers);
        int[] sortedUnevenNumbers = sortArrayBubble(unevenNumbers);

        evenNumIndex = 0;
        unevenNumIndex = sortedUnevenNumbers.length - 1;

        int[] sortedArray = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++){
            if (i % 2 == 0){
                sortedArray[i] = sortedEvenNumbers[evenNumIndex];
                evenNumIndex++;
            }
            else{
                sortedArray[i] = sortedUnevenNumbers[unevenNumIndex];
                unevenNumIndex--;
            }
        }

        return sortedArray;
    }

    public int[] fillArrayUniqueNumbers(){
        int[] arrayWithUniqueNumbers = new int[20];
        for(int i = 0; i < arrayWithUniqueNumbers.length; i++){
            while(true){
                int randomNumber = random.nextInt(30);
                if (checkIfNumberInArray(randomNumber, arrayWithUniqueNumbers)){
                    continue;
                }
                arrayWithUniqueNumbers[i] = randomNumber;
                break;
            }
        }
        return arrayWithUniqueNumbers;
    }

    private boolean checkIfNumberInArray(int number, int[] array){
        for (int i = 0; i < array.length; i++){
            if(array[i] == number){
                return true;
            }
        }
        return false;
    }


    public int[] doTask5(int[] array){
        for (int i = 0; i < array.length; i++){
            if (array[i] % 10 == 4){
                array[i] /= 2;
            }
            else if (array[i] % 2 == 0){
                array[i] *= array[i];
            }
            else{
                array[i] *= 2;
            }
        }
        return array;
    }

    private int[] sortArrayBubble(int[] array){
        boolean notSorted = true;
        while (notSorted) {
            notSorted = false;
            for(int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    int x = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = x;
                    notSorted = true;
                }
            }
        }
        return array;
    }
}
