package test.java.com.practice;

import org.junit.Assert;
import org.junit.Test;

public class ArraysTasksTests {
    ArraysTasks arraysTasks = new ArraysTasks();

    @Test
    public void getMinAndMaxInArrayTest1(){
        int[][][] inputArray = {
                {
                    {3, 4},
                    {2, 44}
                },
                {
                    {-35, 41},
                    {23, 445}
                },
                {
                    {-35, 4},
                    {203, -1000}
                }
        };
        MinAndMax result = arraysTasks.getMinAndMaxInArray(inputArray);
        MinAndMax expected = new MinAndMax(-1000, 445);
        Assert.assertEquals(result, expected);
    }

    @Test
    public void getMinAndMaxInArrayTest2(){
        int[][][] inputArray = {
                {
                        {3, 6},
                        {2, -4}
                },
                {
                        {-35, 41},
                        {23, 45}
                },
                {
                        {-35, 0},
                        {23, -10}
                }
        };
        MinAndMax result = arraysTasks.getMinAndMaxInArray(inputArray);
        MinAndMax expected = new MinAndMax(-35, 45);
        Assert.assertEquals(result, expected);
    }

    @Test
    public void getMinAndMaxInArrayTest3(){
        int[][][] inputArray = {
                {
                        {3, 6},
                        {2, -4},
                        {5, 17}
                },
                {
                        {-35, 41},
                        {23, 45},
                        {34, -98}
                },
                {
                        {-35, 4},
                        {23, -10}
                }
        };
        MinAndMax result = arraysTasks.getMinAndMaxInArray(inputArray);
        MinAndMax expected = new MinAndMax(-98, 45);
        Assert.assertEquals(result, expected);
    }

    @Test
    public void sortArrayTask3RulesTest1(){
        int[] arrayToSort = {3, 4, 5, 7, 0, 44, 56, 1};
        int[] result = arraysTasks.sortArrayTask3Rules(arrayToSort);
        int[] expected = {0, 7, 4, 5, 44, 3, 56, 1};
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void sortArrayTask3RulesTest2(){
        int[] arrayToSort = {};
        int[] result = arraysTasks.sortArrayTask3Rules(arrayToSort);
        int[] expected = {};
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void sortArrayTask3RulesTest3(){
        int[] arrayToSort = {3, 14, 5, 7, 0, 44, 56, 1, 7, 0};
        int[] result = arraysTasks.sortArrayTask3Rules(arrayToSort);
        int[] expected = {0, 7, 0, 7, 14, 5, 44, 3, 56,1};
        Assert.assertArrayEquals(expected, result);
    }
}
