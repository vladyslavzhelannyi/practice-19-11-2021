package main.java.com.practice;

public class Main {
    public static void main(String[] args) {
        ArraysTasks arraysTasks = new ArraysTasks();
//        arraysTasks.outputArray7Rows7Columns();

        int[] arrayTask3Input = {3, 4, 5, 7, 0, 44, 56, 1};
        int[] sortedArrayTask3 = arraysTasks.sortArrayTask3Rules(arrayTask3Input);
        for (int number : sortedArrayTask3){
            System.out.print(number + " ");
        }

//        int[] arrayWithUniqueNumbers = arraysTasks.fillArrayUniqueNumbers();
//        for (int uniqueNumber : arrayWithUniqueNumbers){
//            System.out.print(uniqueNumber + " ");
//        }

//        int[] arrayForTask5 = {3, 4, 45, 6, 76, 103, 0, -23, -4, -32};
//        int[] arrayTask5Output = arraysTasks.doTask5(arrayForTask5);
//        for(int i : arrayTask5Output){
//            System.out.print(i + " ");
//        }
    }
}
