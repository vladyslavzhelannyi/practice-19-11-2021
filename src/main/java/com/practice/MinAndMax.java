package main.java.com.practice;

import java.util.Objects;

public class MinAndMax {
    private int minValue;
    private int maxValue;

    public MinAndMax(int minValue, int maxValue){
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public int getMinValue() {
        return minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MinAndMax minAndMax = (MinAndMax) o;
        return minValue == minAndMax.minValue && maxValue == minAndMax.maxValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(minValue, maxValue);
    }
}
